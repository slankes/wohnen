---
comments: false
gallery: true
---

Das Haus entsteht im beliebten Wohngebiet "Seilerwall" in fußläufiger Nähe zum Stadtzentrum Viersen. 
Das Wohngebiet zeichnet sich durch eine ruhige Lage zwischen den verkehrsführenden Straßen aus. 
Das Haus selbst befindet sich in einer ruhigen anwohnergenutzten Spielstraße.

Ein Discounter, ein Bäcker, ein Tierfuttermarkt sowie ein Drogeriemarkt sind 5 Gehminuten entfernt. Kindergärten, Grund- und weiterführende Schulen sind ebenfalls fußläufig erreichbar. 
Es befinden sich 2 Spielplätze in direkter Umgebung. Diese sind in der Regel aber garnicht nötig, denn auf dieser Spielstraße wird wirklich noch gespielt! 
***

 * Altengerecht gebaut
 * Aufzug bis in den Keller
 * geringe Nebenkosten durch Mieterstrom & Wärmepumpe
 * Gartennutzung

---

<p style="text-align: center;">
<a href="/page/wohnen">[Zu den Wohnungen]</a>
</p>

---

{{< gallery directory="animation*.png" >}}

