---
title: Mieten ...
subtitle: nächste Schritte
comments: false
---

Hallo,

schön, dass Du Dich für die Miete einer der angebotenen Wohungen interessierst.

Um Dir uns uns Zeit zu sparen, hier eine Beschreibung der nächsten Schritte.

- Wir vereinbaren gerne einen Besichtigungstermin, möchten aber als Voraussetzung dafür eine Mieterselbstauskunft von Dir haben.
  - Ein PDF mit der auszufüllenden Auskunft, findest Du [hinter diesem Link](/files/mieterselbstauskunft.pdf)
- Falls beide Seiten danch ein Mietverhältnis anstreben, benötigen wir noch die folgenden zusätzlichen Informationen:
  - Vorlage des Personalausweises
  - Vorlange von Einkommensnachweisn
  - SCHUFA-Auskunft

