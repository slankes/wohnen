---
title: Wohnen
subtitle: Die Wohnungen
comments: false
gallery: true
---

## Allen Wohnungen ist gemein

 * schwellenfreier Zugang zu allen Wohnungen und dem Keller durch bequeme Aufzugsanlage
 * eigenes Kellerabteil
 * Extra Abstellfläche im Keller für Kinderwagen und Rollatoren
 * Extra Waschkeller für Waschmaschine & Trockner
 * elektrische Türöffner mit Videogegensprechanlage
 * Die Beheizung der Wohnungen erfolgt über eine effiziente Luft-Wärmepumpe
 * Photovoltaik-Anlage zur Heizungsunterstützung, für den Allgemeinstrom und für günstigen Mieterstrom
 * komplett eingerichtetes Tageslicht-Bad mit Unterschrank, Spiegel, Spiegelleuchte und ebenerdiger Dusche mit Glas-Trennwand
 * helle Räume durch bodentiefe Fenster
 * Fenster sind 3-fach verglast für optimale Energie-Effzienz und höchsten Schallschutz
 * moderne und robuste Bodenbeläge, die zu vielen Einrichtungsstylen passen
 * großzügige Balkone mit Edelstahlgeländern
 * lichtdurchflutetes Treppenhaus
 * Ein Stellplatz auf dem Grundstück kann zusätzlich angemietet werden.  
 * Lademöglichkeit für Elektroautos
 * Die gemeinschaftliche Gartennutzung ist selbstverständlich. Ein Mährobotor übernimmt die Rasenpflege. Beete können auf Wunsch mitgestaltet werden. 

## Erdgeschoss, eine Wohnung mit Terasse. Mitten in der Stadt und trotzdem ruhig.

 * Grundriss
   * Entree
   * WC
   * Abstellraum
   * Küche und Wohnen
   * Kind
   * Bad
   * Schlafzimmer
   * Terrasse
   * Abstellraum im Keller
   * **Gesamt 101.50 m2**  
  
 * Waschmaschine kann, trotz Abstellraum mit vorhandenen Anschlüssen, bei Bedarf auch im vorgesehenem Waschkeller untergebracht werden.
  
{{< img src="eg.png" link="eg.png" alt="Erdgeschoss" width="800px">}}

## 1. OG Links, helle Single Wohnung mit Style

{{< rawhtml >}}<p style="color: red"><strong>Vermietet</strong></p>{{< / rawhtml >}}

 * Grundriss
   * Wohnküche
   * Bad
   * Empore
   * WC Empore
   * Abstellraum im Keller
   * Balkon
   * **Gesamt 51 m2**

{{< img src="1oglinks.png" link="1oglinks.png" alt="1. OG Links" width="800px">}}

## 1. OG Rechts, helle 3 Zimmer Wohnung mit großem Balkon und Gartennutzung

{{< rawhtml >}}<p style="color: red"><strong>Vermietet</strong></p>{{< / rawhtml >}}

 * Grundriss
   * Entree
   * Wohnküche
   * Bad
   * WC
   * Schlafzimmer
   * Kinderzimmer
   * Abstellraum im Keller
   * Balkon
   * **Gesamt 94 m2**

{{< img src="1ogrechts.png" link="1ogrechts.png" alt="1. OG Links" width="800px">}}

## 2. OG mit Empore

### 2. OG

 * Grundriss
   * Entree
   * Bad
   * WC
   * Küche/Wohnen
   * Schlafen
   * Empore
   * Balkon
   * Abstellraum im Keller
   * **Gesamt 140 m2**

{{< img src="2og.png" link="2og.png" alt="2. OG" width="800px">}}

### Empore

{{< img src="3og.png" link="3og.png" alt="3. OG - Empore" width="800px">}}

## Keller mit Homeoffice-Raum

{{< img src="keller.png" link="keller.png" alt="Keller" width="800px">}}

